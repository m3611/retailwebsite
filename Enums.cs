﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailWebsite
{
    public enum System_State
    {
        IDLE,
        PERC_30,
        PERC_20,
        PERC_10,
        OVER_2_YEARS,
        DOLLAR_200,
        OUTPUT_STATE
    }

    public enum Command
    {
        NEXT_STATE,
        BREAK,
    }

    public enum Card_Type
    {
        GOLD,
        SILVER,
        NONE
    }

    public enum Category
    {
        PHONE,
        OTHER
    }

    public static class Helper
    {

    }
}
