﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailWebsite
{
    public interface IState
    {
        System_State State      { get; set; }
        System_State Next_State { get; set; }
        Func<Command> Function  { get; set; }
        Command Update();
    }
}
