﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailWebsite
{
    public class ConditionChecker
    {
        private User    user;
        private Product product;

        public ConditionChecker(User user, Product product)
        {
            this.user    = user;
            this.product = product;
        }
        /*********************************************************************/
        public Command UserHasGoldCard()
        {
            if(user.Card == Card_Type.GOLD)
            {
                product.Price = product.Price * 0.7;
                return Command.BREAK;
            }
            else
            {
                return Command.NEXT_STATE;
            }
        }
        /*********************************************************************/
        public Command UserHasSilverCard()
        {
            if (user.Card == Card_Type.SILVER)
            {
                product.Price = product.Price * 0.8;
                return Command.BREAK;
            }
            else
            {
                return Command.NEXT_STATE;
            }
        }
        /*********************************************************************/
        public Command UserHasOverTwoYears()
        {
            if (true == user.IsOverTwoYears)
            {
                product.Price = product.Price * 0.95;
                return Command.BREAK;
            }
            else
            {
                return Command.NEXT_STATE;
            }
        }
        /*********************************************************************/
        public Command UserIsAffiliate()
        {
            if (true == user.IsAffiliate)
            {
                product.Price = product.Price * 0.90;
                return Command.BREAK;
            }
            else
            {
                return Command.NEXT_STATE;
            }
        }
        /*********************************************************************/
        public Command Dollar_200()
        {
            int remain = (int)product.Price / 200;
            int discount = remain * 5;
            product.Price -= discount;
            return Command.BREAK;
        }
        /*********************************************************************/
        public Command Idle()
        {
            return Command.NEXT_STATE;
        }
    }
}
