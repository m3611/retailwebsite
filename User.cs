﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RetailWebsite
{
    public class User
    {
        public Card_Type Card      = Card_Type.NONE;
        public bool IsAffiliate    = false;
        public bool IsOverTwoYears = false;

        public User(Card_Type Card, bool IsAffiliate, bool IsOverTwoYears)
        {
            this.Card = Card;
            this.IsAffiliate  =  IsAffiliate;
            this.IsOverTwoYears = IsOverTwoYears;
        }

        public User() { }
    }
}
